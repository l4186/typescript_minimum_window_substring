# golang minimum window substring

Given two strings s and t of lengths m and n respectively, return the minimum window substring of s such that every character in t (including duplicates) is included in the window. If there is no such substring, return the empty string "".

The testcases will be generated such that the answer is unique.

A substring is a contiguous sequence of characters within the string.

## Examples

```
Example 1:

Input: s = "ADOBECODEBANC", t = "ABC"
Output: "BANC"
Explanation: The minimum window substring "BANC" includes 'A', 'B', and 'C' from string t.
Example 2:

Input: s = "a", t = "a"
Output: "a"
Explanation: The entire string s is the minimum window.
Example 3:

Input: s = "a", t = "aa"
Output: ""
Explanation: Both 'a's from t must be included in the window.
Since the largest window of s only has one 'a', return empty string.
```

## 解析

這個題目需要從 s 找出包含具有 t 所有字元的子字串

所以需要把 t 的字元出現次數統計出來

並且然後每次當 slide window 字元統計次數 有達到 t 的字元出現次數則開始把左界收斂到最少字串

然後在逐步把右界向右

## 程式碼

```typescript
export const minWindow = (s: string, t: string): string => {
  const sLen = s.length;
  const tLen = t.length;
  if (sLen == 0 || tLen == 0 || sLen < tLen) {
      return "";
  }
  const tFreq: Array<number> = new Array<number>(256);
  const sFreq: Array<number> = new Array<number>(256);
  tFreq.fill(0);
  sFreq.fill(0);
  
  // collect all tFreq
  for (let idx = 0 ; idx < tLen; idx++) {
    tFreq[t[idx].charCodeAt(0)]++;
  }
  let right = -1; // 從最左邊開始
  let left = 0;
  let finalRight = -1;
  let finalLeft = -1;
  let minW = sLen + 1; // 給一個極大值 當初始值
  let result = "";
  let count = 0;// count current slide window
  while (left < sLen) { // 左界還沒到最後
    if (right + 1 < sLen && count < tLen) {// check for count arrive minimum 
      sFreq[s[right+1].charCodeAt(0)]++;
      if (sFreq[s[right+1].charCodeAt(0)] <= tFreq[s[right+1].charCodeAt(0)]) {
          count++;
      }
      right++;
    } else {
      if (right - left + 1 < minW && count == tLen) {
          minW = right - left + 1;
          finalRight = right;
          finalLeft = left;
      }
      if (sFreq[s[left].charCodeAt(0)] == tFreq[s[left].charCodeAt(0)]) { // 更新 
          count--;
      }
      sFreq[s[left].charCodeAt(0)]--;
      left++;
    }   
  }  
  if (finalLeft != -1) {
    result = s.slice(finalLeft, finalRight + 1);
  }
  return result;  
};
```