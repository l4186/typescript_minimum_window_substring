import { minWindow } from "./solution";

describe('minimum_window_substring', () => {
  it('Example1', () => {
    expect(minWindow("ADOBECODEBANC","ABC")).toEqual("BANC");
  });
  it('Example2', () => {
    expect(minWindow("a","a")).toEqual("a");
  });
  it('Example3', () => {
    expect(minWindow("a","aa")).toEqual("");
  });
});