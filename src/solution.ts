export const minWindow = (s: string, t: string): string => {
  const sLen = s.length;
  const tLen = t.length;
  if (sLen == 0 || tLen == 0 || sLen < tLen) {
      return "";
  }
  const tFreq: Array<number> = new Array<number>(256);
  const sFreq: Array<number> = new Array<number>(256);
  tFreq.fill(0);
  sFreq.fill(0);
  
  // collect all tFreq
  for (let idx = 0 ; idx < tLen; idx++) {
    tFreq[t[idx].charCodeAt(0)]++;
  }
  let right = -1; // 從最左邊開始
  let left = 0;
  let finalRight = -1;
  let finalLeft = -1;
  let minW = sLen + 1; // 給一個極大值 當初始值
  let result = "";
  let count = 0;// count current slide window
  while (left < sLen) { // 左界還沒到最後
    if (right + 1 < sLen && count < tLen) {// check for count arrive minimum 
      sFreq[s[right+1].charCodeAt(0)]++;
      if (sFreq[s[right+1].charCodeAt(0)] <= tFreq[s[right+1].charCodeAt(0)]) {
          count++;
      }
      right++;
    } else {
      if (right - left + 1 < minW && count == tLen) {
          minW = right - left + 1;
          finalRight = right;
          finalLeft = left;
      }
      if (sFreq[s[left].charCodeAt(0)] == tFreq[s[left].charCodeAt(0)]) { // 更新 
          count--;
      }
      sFreq[s[left].charCodeAt(0)]--;
      left++;
    }   
  }  
  if (finalLeft != -1) {
    result = s.slice(finalLeft, finalRight + 1);
  }
  return result;  
};